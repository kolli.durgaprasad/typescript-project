console.log('hello world');

console.log('Sum');
function getSum(x:number, y:number) {
  return  x + y;
}
console.log(getSum(1, 3));

console.log('min2');
function min2(x:number, y:number):number {
  if (x < y)
    return x;
    return y;  
}
console.log(min2(20, 30));

console.log('max2');
function max2(x:number, y:number):number {
  if (x > y)
    return x;
    return y;
}
console.log(max2(50,100));

console.log('max3');
function max3(x:number, y:number, z:number):number {
  if (x > y){
    return x;
  }
  else if(y > z){
    return y;
  }
  return z;
}
console.log(max3(20, 40, 30));

console.log('min3');
function min3(x:number, y:number, z:number):number {
  if (x < y && x < z) { 
    return x;
  }
  else if (y < z) {
    return y;
  }
  return z;
}
console.log(min3(30, 40, 20 ));

console.log('isFactor');
function isFactor1(n:number, i:number):boolean {
  if (n % i == 0)
    return true;
  else 
    return false;
}
console.log(isFactor1(4, 2));
console.log(isFactor1(3, 2));

console.log('isEven');
function isEven1(n:number):string{
  if (n % 2 == 0)
    return 'Even';
  else
    return 'Odd';
}
console.log(isEven1(2));
console.log(isEven1(1));

console.log('isOdd');
function isOdd(n:number):string {
  if (n % 2 != 0)
    return 'Even';
  else
    return 'Odd';
}
console.log(isOdd(2));
console.log(isOdd(1));

console.log('isLeapYear');
function isLeap1(year:number):string {
  if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 !== 0)))
    return 'Leap Year'; 
  else {
    return 'Not-Leap Year';
  }
}
console.log(isLeap1(2016));
console.log(isLeap1(2017));

console.log('licsBetween');
function licsBetween1(m:number, x:number,y:number) :boolean {
  if (m > x && m < y)
    return true;
  else
    return false;
}
console.log(licsBetween1(5,0,10));
console.log(licsBetween1(0, 5, 10));

console.log('indexOf(arr, value)');
function findIndexInarr(arr:[0,1,2,3], value:number ): number {
  for (let i = 0; i < arr . length; i++) {
    if (arr[i] == value) {
      return i;
    }
  }
  return -1;
}
console.log(findIndexInarr([0,1,2,3], 4));

console.log('Power');
function power(x:number, y:number):number {
 let a = 1;
  for (let i = 1; i <= y; ++i)
   a = a *= x ;
   return a;
}
console.log(power(100,10));

console.log('ncr');
function ncr1(n:number, r:number):number {
  return factorial(n) / (factorial(n - r) * factorial(r));
}
console.log(ncr1(2, 4));

console.log('squareAll(arr)');
function squareAll(arr: number[]): number[] {
  const a = [ ];
  for (let i = 0; i < arr . length;  ++i) {
  a.push(arr[i] * arr[i]);
  }
  return a;
}
console.log(squareAll([10, 20, 30, 40, 50]))

console.log('allEven(arr)');
function allEven(arr: number[]): number[] {
  const b = [ ];
  for (let i = 0; i <= arr . length; ++i) {
  // b.push(arr[i] % 2 === 0);
    if (arr[i] % 2 ===0){
      b.push(arr[i]);
    }
  }
  return b;
}
console.log(allEven([1, 2, 3, 4, 5, 6]));

console.log('concat');
function concat(x: number[], y: number[]): number[] {
  for (let i = 0; i < y . length; ++i){
  x.push(y[i]);
  }
  return x;
}
console.log(concat([1, 2, 3, 4, 5], [6, 7, 8, 9, 10]));

console.log('max(arr)');
function maxarr(arr: number[]): number {
  let m = 0;
  for (let i=0; i <= arr . length; ++i) {
    if (m < arr[i]) {
    m = arr[i];
    }
  }
  return m;
}
console.log(maxarr([100, 200, 300, 400, 500]));

// const factorial = (n: number): number => return((x, y) => x * y, 1);

const product = (arr: number[]): number => arr.reduce((x, y) => x * y, 1);