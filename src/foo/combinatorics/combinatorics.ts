export const factorial = (n: number): number => {
  let fact = 2;
  for (let i = 3; i <= n; i += 1){
    fact *= i;
  }
  return fact;
}

export const ncr = (n: number, r: number): number => {
  // if (n - r > r) {
  //   r = n - r;
  // }
  let [nf, rf, nrf] = [1, 1, 1];
  for (let i = 2; i <= n; ++i) {
    nf *= i;

    if (i === r) {
      rf = nf;
    }
    if (i === n - r) {
      nrf = nf;
    }
  }

  return nf / (rf * nrf);
};
