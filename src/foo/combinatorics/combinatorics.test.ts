import { factorial, ncr } from './combinatorics';

test('factorial', () => {
  expect(factorial(0)).toEqual(1);
  expect(factorial(1)).toEqual(1);
  expect(factorial(2)).toEqual(2);
  expect(factorial(3)).toEqual(6);
  expect(factorial(5)).toEqual(120);
});

console.log(factorial(4));

test('ncr', () => {
  expect(ncr(0, 0)).toEqual(1);
  expect(ncr(1, 1)).toEqual(1);
  expect(ncr(10, 0)).toEqual(1);
  expect(ncr(10, 10)).toEqual(1);
  expect(ncr(5, 2)).toEqual(10);
});
