export const ncr = (n: number, r: number): number => {
  let [nf, rf, nrf] = [1, 1, 1];
  for (let i = 2; i <= n; ++i) {
    nf *= i;

    if (i === r) {
      rf = nf;
    }
    if (i === n - r) {
      nrf = nf;
    }
  }
  return nf / (rf * nrf);
};