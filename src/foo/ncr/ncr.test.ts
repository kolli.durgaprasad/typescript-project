import { ncr } from './ncr';

test('ncr', () => {
  expect(ncr(0, 0)).toEqual(1);
  expect(ncr(1, 1)).toEqual(1);
  expect(ncr(10, 0)).toEqual(1);
  expect(ncr(10, 10)).toEqual(1);
  expect(ncr(5, 2)).toEqual(10);
});