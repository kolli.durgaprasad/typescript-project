import { maxarr2 } from './maxarr';
test('maxarr2', () => {
  expect(maxarr2([100, 200, 300, 400, 500])).toEqual(500);
});