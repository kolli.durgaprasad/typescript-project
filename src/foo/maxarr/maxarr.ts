export const maxarr2 = (arr: number[]): number => {
  let m = arr[0];
  for (let i = 1; i < arr . length; ++i){
    if (m < arr[i]){
      m = arr[i];
    }
  }
  return m;
}