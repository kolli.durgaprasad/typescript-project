export const factorial = (n: number): number => {
  let fact = 2;
  for (let i = 3; i <= n; i += 1) {
    fact *= i;
  }
  return fact;
}
