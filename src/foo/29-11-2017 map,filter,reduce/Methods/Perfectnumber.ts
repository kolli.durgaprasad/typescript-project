const range = (start: number, stop: number): ReadonlyArray<number> => {
  const a: number[] = [];
  for (let i = start; i <= stop; ++i) {
    a.push(i);
  } return a;
}
console.log(range(0, 10));

const isPerfect = (n: number) => factors(n).reduce((x, y) => x + y) === (2 * n);
console.log(isPerfect(6));

// PERFECTNUMBER....................
const perfectnumber = (start: number, stop: number): ReadonlyArray<number> => range(start, stop).filter(n => isPerfect(n));
console.log(perfectnumber(1, 10000));
// .................................