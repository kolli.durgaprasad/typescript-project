const range = (start: number, stop: number): ReadonlyArray<number> => {
  const a: number[] = [];
  for (let i = start; i <= stop; ++i) {
    a.push(i);
  } return a;
}
console.log(range(0, 10));

const pascalLine = (n: number): number[] => range(0, n).map(r => ncr(n, r));
console.log(pascalLine(2));

// PASCALTRIANGAL.................
const pascalTriangle = (lines: number): ReadonlyArray<ReadonlyArray<number>> => range(0, lines + 1).map(line => pascalLine(line));
console.log(pascalTriangle(1));
// ................................