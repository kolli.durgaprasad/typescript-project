const isPrime = (n: number): boolean => factors(n).length === 2 ? true : false;
console.log(isPrime(5));

// PRIMENUMBER.......................
const primeNumber = (start: number, stop: number): ReadonlyArray<number> => range(start, stop).filter(n => isPrime(n));
console.log(primeNumber(1, 100));
// ...................................