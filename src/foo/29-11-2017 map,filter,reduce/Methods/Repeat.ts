const range = (start: number, stop: number): ReadonlyArray<number> => {
  const a: number[] = [];
  for (let i = start; i <= stop; ++i) {
    a.push(i);
  } return a;
}
console.log(range(0, 10));

// REPEAT.........................
const repeat = (x: number, n: number): ReadonlyArray<number> => range(1, n).map(v => v = x)
console.log(repeat(1, 10));
// ................................