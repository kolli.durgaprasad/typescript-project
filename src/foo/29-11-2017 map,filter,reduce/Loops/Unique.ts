// UNIQUE .........................
const unique = (arr: ReadonlyArray<number>): ReadonlyArray<number> => {
  const Uni = [];
  let U = 0;
  for (let i = 0; i < arr.length; i++) {
    U++;
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[i] === arr[j]) {
        U++;
      }
    }
    if (U === 1) {
      Uni.push(arr[i]);
    }
    U = 0;
  }
  return Uni;
};
console.log(unique([10, 20, 10, 30, 40, 20, 50]));
// ................................
