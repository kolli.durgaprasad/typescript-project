// DROPWHILE.....................
const dropWhile = (arr: number[], E: (x: number) => boolean): ReadonlyArray<number> => {
  for (let i = 0; i < arr.length; i++) {
    if (!E(arr[i])) {
      if (E(arr[i + 1]))
        return arr.slice(i)
    }
  }
  return arr;
}
console.log(dropWhile([2, 4, 6, 9, 12], x => x % 2 === 0));
// ..............................