// CONCAT........................
const concat = (arr1: ReadonlyArray<number>, arr2: ReadonlyArray<number>): ReadonlyArray<number> => {
  for (const e of arr2) {
    arr1 = [...arr1, e];
  }
  return arr1;
}
console.log(concat([10, 20, 30, 40, 50], [60, 70, 80, 90, 100]));
//................................