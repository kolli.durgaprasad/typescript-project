// ZIP............................
const zip = (arr1: ReadonlyArray<number>, arr2: ReadonlyArray<number>) => {
  const Z = [];
  for (let i = 0; i < arr1.length; i = i + 1) {
    Z.push([arr1[i], arr2[i]]);
  }
  return [Z];
};
console.log(zip([1, 2, 3], [4, 5, 6]));
// ...............................