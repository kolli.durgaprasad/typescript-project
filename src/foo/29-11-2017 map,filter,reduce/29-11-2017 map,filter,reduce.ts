// const range = (start: number, stop: number): ReadonlyArray<number> => {
//   const a: number[] = [];
//   for (let i = start; i <= stop; ++i) {
//     a.push(i);
//   } return a;
// }
// console.log(range(0, 10));

// const factors = (n: number): number[] => range(1, n).filter(i => n % i === 0);
// console.log(factors(6));

// const isPerfect = (n: number) => factors(n).reduce((x, y) => x + y) === (2 * n);
// console.log(isPerfect(6));

// // PERFECTNUMBER....................
// const perfectnumber = (start: number, stop: number): ReadonlyArray<number> => range(start, stop).filter(n => isPerfect(n));
// console.log(perfectnumber(1, 10000));
// // .................................

// const isPrime = (n: number): boolean => factors(n).length === 2 ? true : false;
// console.log(isPrime(5));

// // PRIMENUMBER.......................
// const primeNumber = (start: number, stop: number): ReadonlyArray<number> => range(start, stop).filter(n => isPrime(n));
// console.log(primeNumber(1, 100));
// // ...................................

// const factorial = (n: number): number => range(1, n).reduce((x, y) => x * y, 1);
// console.log(factorial(4));

// const ncr = (n: number, r: number) => factorial(n) / (factorial(r) * factorial(n - r));
// console.log(ncr(5, 2));

// const pascalLine = (n: number): number[] => range(0, n).map(r => ncr(n, r));
// console.log(pascalLine(2));

// // PASCALTRIANGAL.................
// const pascalTriangle = (lines: number): ReadonlyArray<ReadonlyArray<number>> => range(0, lines + 1).map(line => pascalLine(line));
// console.log(pascalTriangle(1));
// // ................................

// // REPEAT.........................
// const repeat = (x: number, n: number): ReadonlyArray<number> => range(1, n).map(v => v = x)
// console.log(repeat(1, 10));
// // ................................

// // POWER..........................
// const Power = (x: number, n: number): number => repeat(x, n).reduce((x, n) => x * n);
// console.log(Power(2, 4));
// // ................................

// // MAX............................
// const maxarr = (arr: number[]): number => arr.reduce((x, y) => x > y ? x : y);
// console.log(maxarr([1, 2, 3, 4, 5, 6]))
// // ................................