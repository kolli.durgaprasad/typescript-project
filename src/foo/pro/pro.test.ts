import { min } from "./pro";
test('min', () => {
  expect(min(100, 200)).toEqual(200);
  expect(min(100, 100)).toEqual(100);
});

import { isFactor } from "./pro";
test('isFactor', () => {
  expect(isFactor(2, 4)).toEqual(true);
});

import { isEven } from "./pro";
test('isEven', () => {
  expect(isEven(1)).toEqual(false);
  expect(isEven(200)).toEqual(false);
  expect(isEven(3)).toEqual(false);
  expect(isEven(450)).toEqual(false);
  expect(isEven(5)).toEqual(false);
  expect(isEven(666)).toEqual(false);
});

import { isLeap } from "./pro";
test('isleap', () => {
  expect(isLeap(2017)).toEqual(true);
  expect(isLeap(2017)).toEqual(false);
});

import { licsBetween } from "./pro";
test('liceBetween', () => {
  expect(licsBetween(2, 1, 3)).toEqual(true);
  expect(licsBetween(0, 5, 10)).toEqual(false);
});

import { maxarr2 } from './pro';
test('maxarr2', () => {
  expect(maxarr2([100, 200, 300, 400, 500])).toEqual(500);
});

import { indexof } from './pro';
test('indexof', () => {
  expect(indexof([1, 2, 3], 10)).toEqual(-1);
});

// test('jest', () => {
//   expect(true).toBeTruthy();
// });

import { factorial, ncr } from './pro';
test('factorial', () => {
  expect(factorial(4)).toEqual(24);
});

test('ncr', () => {
  expect(ncr(0, 0)).toEqual(1);
  expect(ncr(1, 1)).toEqual(1);
  expect(ncr(10, 0)).toEqual(1);
  expect(ncr(10, 10)).toEqual(1);
  expect(ncr(5, 2)).toEqual(10);
});

import  range  from './pro';
test('range', () => {
  expect(range(1, 5)).toEqual([1, 2, 3, 4]);
});