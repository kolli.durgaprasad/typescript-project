export const add = (x: number, y: number) => x + y ; 
console.log(add(100, 200));

export const min = (x: number, y: number) => (x < y) ?  x : y;
console.log(min(100, 200));

export const isFactor = (n: number, i:number): boolean => n % i == 0;
console.log(isFactor(4, 2));

export const isEven = (n: number): boolean => n % 2 == 0;
console.log(isEven(2));
console.log(isEven(1));

export const isLeap = (year: number): boolean => ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 !== 0)));
console.log(isLeap(2016));
console.log(isLeap(2017));

export const licsBetween = (n: number, x: number, y:number):boolean => (x < n && y > n);
console.log(licsBetween(5, 0, 10));
console.log(licsBetween(0, 5, 10));

export const maxarr2 = (arr: number[]): number => {
  let m = arr[0];
  for (let i = 1; i < arr . length; ++i){
    if (m < arr[i]){
      m = arr[i];
    }
  }
  return m;
}
console.log(maxarr2([100, 200, 300, 400, 500]));

export const indexof = (arr: number[], value: number): number => {
  for  (let i = 0; i < arr . length; i++){
    if (arr[i] == value) {
      return i;
    }
  }
  return -1;
}

// import { plus, mines } from './operators';

// console.log(plus(1, 2), mines(3, 4));

export const factorial = (n: number): number => {
  let fact = 2;
  for (let i = 3; i <= n; i += 1) {
    fact *= i;
  }
  return fact;
}

export const ncr = (n: number, r: number): number => {
  let [nf, rf, nrf] = [1, 1, 1];
  for (let i = 2; i <= n; ++i) {
    nf *= i;

    if (i === r) {
      rf = nf;
    }
    if (i === n - r) {
      nrf = nf;
    }
  }
  return nf / (rf * nrf);
};

const range = (start: number, stop: number): number[] => {
  const a: number[] = [];
  for (let i = start; i < stop; ++i) {
    a . push(i);
  }
  return a;
};
export default range;