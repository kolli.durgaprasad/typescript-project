import { plus, minus} from './operators';
test('plus' , () => {
  expect(plus(1, 2)).toEqual(3);
});
test('minus', () => {
  expect(minus(1, 2)).toEqual(3);
});
