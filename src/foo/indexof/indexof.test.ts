import { indexof } from './indexof';
test('indexof', () => {
  expect(indexof([1, 2, 3], 10)).toEqual(-1);
});