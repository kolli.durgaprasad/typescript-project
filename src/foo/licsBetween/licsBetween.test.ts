import { licsBetween } from "./liseBetween";
test('liceBetween', () => {
  expect(licsBetween(2, 1, 3)).toEqual(true);
  expect(licsBetween(0, 5, 10)).toEqual(false);
});