const range = (start: number, stop: number): number [] => {
  const a: number[] = [];
  for (let i = start; i < stop; ++i) {
    a . push(i);
  }
  return a;
};
export default range;